<?php
require_once 'connection.php';

$link = mysqli_connect($host, $user, $password, $database)
  or die ("Помогите" . mysqli_error());

echo "Работает!<br>";

$sql="
CREATE TABLE alp(
alp_name varchar(50) NOT NULL,
alp_address varchar(50) NOT NULL,
PRIMARY KEY (alp_name))";
if (mysqli_query($link, $sql)) {
  echo "Таблица alp создана";
} else {
  echo "Ошибочка alp" . mysqli_error($link);
}

$sql = "INSERT INTO alp VALUES 
('Sanya', 'Grazhdanka'),
('Lexa', 'Kupchino'),
('Dimon', 'Dybenko'),
('Mixalych', 'Bolsevicov'),
('Petya', 'Devyatkino')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные alp<br>";
} else {
  echo "Ошибкаа alp<br>" . mysqli_error($link);
}

$sql = "INSERT INTO alp VALUES 
('Lina', 'Gorcovskaya'),
('Maxa', 'Politex'),
('Eva', 'Arbat'),
('Vika', 'Vavilovyx'),
('Nina', 'Primorsky')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные alp<br>";
} else {
  echo "Ошибкаа alp<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE club(
club_name varchar(20) NOT NULL,
club_number varchar(20) NOT NULL,
club_country varchar(20) NOT NULL,
club_town varchar(20) NOT NULL,
club_person varchar(50) NOT NULL,
club_mail varchar(50) NOT NULL,
PRIMARY KEY (club_name))";
if (mysqli_query($link, $sql)) {
  echo "Таблица club создана";
} else {
  echo "Ошибочка club" . mysqli_error($link);
}

$sql = "INSERT INTO club VALUES
('A', '1234567', 'Russia', 'SPb', 'AAA', 'a@gmail.com'),
('B', '1234568', 'Russia', 'Moscow', 'BBB', 'b@gmail.com'),
('C', '1234569', 'Russia', 'SPb', 'CCC', 'c@gmail.com'),
('D', '1234560', 'Russia', 'SPb', 'DDD', 'd@gmail.com'),
('E', '1234561', 'Russia', 'Moscow', 'EEE', 'e@gmail.com')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные club<br>";
} else {
  echo "Ошибкаа club<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE contract(
contract_conditions text NOT NULL,
contract_value varchar(20) NOT NULL,
club_name varchar(20) NOT NULL,
alp_name varchar(50) NOT NULL,
FOREIGN KEY (club_name) REFERENCES club(club_name),
FOREIGN KEY (alp_name) REFERENCES alp(alp_name))";
if (mysqli_query($link, $sql)) {
  echo "Таблица contract создана";
} else {
  echo "Ошибочка contract" . mysqli_error($link);
}

$sql = "INSERT INTO contract VALUES
('ok', '30000', 'A', 'Sanya'),
('okk', '27000', 'B', 'Lexa'),
('ook', '32000', 'C', 'Dimon'),
('ookk', '30000', 'D', 'Mixalych'),
('kk', '25000', 'E', 'Petya')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные contract<br>";
} else {
  echo "Ошибкаа contract<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE groupp(
group_number int(10) NOT NULL,
alp_quantity int(10) NOT NULL,
info text NOT NULL,
PRIMARY KEY (group_number))";
if (mysqli_query($link, $sql)) {
  echo "Таблица groupp создана";
} else {
  echo "Ошибочка groupp" . mysqli_error($link);
}

$sql = "INSERT INTO groupp VALUES
('1', '2', 'a'),
('2', '2', 'b'),
('3', '2', 'c')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные groupp<br>";
} else {
  echo "Ошибкаа groupp<br>" . mysqli_error($link);
}

$sql = "INSERT INTO groupp VALUES
('4', '4', 'd')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные groupp<br>";
} else {
  echo "Ошибкаа groupp<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE form(
alp_name varchar(50) NOT NULL,
group_number int(10) NOT NULL,
alp_skills text NOT NULL,
alp_interests text NOT NULL,
alp_experience text NOT NULL,
FOREIGN KEY (alp_name) REFERENCES alp(alp_name),
FOREIGN KEY (group_number) REFERENCES groupp(group_number))";
if (mysqli_query($link, $sql)) {
  echo "Таблица form создана";
} else {
  echo "Ошибочка form" . mysqli_error($link);
}

$sql = "INSERT INTO form VALUES
('Sanya', '1', 'norm', 'oi', '2 years'),
('Lina', '1', 'norm', 'op', '1.5 years'),
('Lexa', '2', 'okey', 'li', '1 year'),
('Maxa', '2', 'norm', 'lp', '1 year'),
('Dimon', '3', 'great', 'ai', '5 years'),
('Eva', '3', 'great', 'ap', '4.5 years'),
('Mixalych', '4', 'okey', 'di', '3 years'),
('Petya', '4', 'okey', 'dp', '3 years'),
('Vika', '4', 'good', 'dp', '3.5 years'),
('Nina', '4', 'okey', 'di', '3 years')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные form<br>";
} else {
  echo "Ошибкаа form<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE route(
id_route int(10) NOT NULL AUTO_INCREMENT,
route_description text NOT NULL,
PRIMARY KEY (id_route))";
if (mysqli_query($link, $sql)) {
  echo "Таблица route создана";
} else {
  echo "Ошибочка route" . mysqli_error($link);
}

$sql = "INSERT INTO route VALUES
('234', 'hard'),
('235', 'easy'),
('236', 'normm')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные route<br>";
} else {
  echo "Ошибкаа route<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE mountain(
id_route int(10) NOT NULL AUTO_INCREMENT,
mountain_name varchar(50) NOT NULL,
height varchar(20) NOT NULL,
country varchar(50) NOT NULL,
area varchar(50) NOT NULL,
PRIMARY KEY (mountain_name),
FOREIGN KEY (id_route) REFERENCES route(id_route))";
if (mysqli_query($link, $sql)) {
  echo "Таблица mountain создана";
} else {
  echo "Ошибочка mountain" . mysqli_error($link);
}

$sql = "INSERT INTO mountain VALUES
('234', 'Elbrus', '5642', 'Russia', 'Kavkaz'),
('235', 'Beluxa', '4509', 'Russia', 'Ust-K'),
('236', 'Kazbek', '5033', 'Russia', 'S-O')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные mountain<br>";
} else {
  echo "Ошибкаа mountain<br>" . mysqli_error($link);
}

$sql="
CREATE TABLE up(
group_number int(10) NOT NULL,
id_route int(10) NOT NULL AUTO_INCREMENT,
plan_duration varchar(50) NOT NULL,
real_duration varchar(50) NOT NULL,
start date NOT NULL,
finish date NOT NULL,
FOREIGN KEY (group_number) REFERENCES groupp(group_number),
FOREIGN KEY (id_route) REFERENCES route(id_route))";
if (mysqli_query($link, $sql)) {
  echo "Таблица up создана";
} else {
  echo "Ошибочка up" . mysqli_error($link);
}

$sql = "INSERT INTO up VALUES
('1', '235', '7 days', '8 days', '23.05.2001', '31.05.2001'),
('2', '236', '13 days', '13 days', '07.06.2016', '20.06.2016'),
('3', '235', '7 days', '7 days', '09.03.2003', '16.03.2003'),
('4', '234', '14 days', '15 days', '13.09.2016', '28.09.2016')";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные up<br>";
} else {
  echo "Ошибкаа up<br>" . mysqli_error($link);
}

mysqli_close($link);
?>