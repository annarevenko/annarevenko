import math
import random


def group(numbers, groups):
    return [values[i:i+n] for i in range(0, len(values), n)]


def read_sudoku(filename):
    """ Прочитать Судоку из указанного файла """
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def get_row(values, pos):
    return(values[pos[0]])


def get_col(values, pos):
    return [values[i][pos[1]] for i in range(0, len(values))]


def get_block(values, pos):
    numb = []
    a, b = pos[0] // 3, pos[1] // 3
    for i in range(3):
        numb.append(values[a*3 + i][b*3:b*3+3])
    return numb


def find_empty_positions(grid):
    for i in range(len(grid)):
        if (grid[i].count(".") != 0):
            return (i, grid[i].index("."))
    return ()


def find_possible_values(grid, pos):
    numb = set('123456789') 
    row = set(get_row(grid, pos)) 
    col = set(get_col(grid, pos)) 
    block = set(get_block(grid, pos)) 
    return numb - set.union(block, row, col)
    

def solve(grid):
    b = find_empty_positions(grid)
    if b == ():
        return grid
    else:
        a = find_possible_values(grid, b)
        if a == []:
            return None
        for i in a:
            grid[b[0]][b[1]] = str(i)
            s = solve(grid)
            if s is not None:
                return grid
    grid[b[0]][b[1]] = "."


def check_solution(grid):
    su = set([str(i) for i in range(1,10)])
    for j in range(9):
        row = set(get_row(grid, (j, 0)))
        col = set(get_col(grid, (0, j)))
        if row != su:
            return False
        if col != su:
            return False
    for j in range(3):
        for k in range(3):
            block = set(get_block(grid,(j*3, k*3))[0] + get_block(grid,(j*3, k*3))[1] + get_block(grid,(j*3, k*3))[2])
            if block != su:
                return False
    return True
 

def generate_sudoku(n):
    new_sudoku = []
    for row in range(9): 
        new_sudoku.append([])
        for col in range(9):
            new_sudoku[row].append(".") 
    new_sudoku[0][0] = str(random.randrange(1, 9)) 
    new_sudoku[1][8] = str(random.randrange(1, 9))
    new_sudoku[3][4] = str(random.randrange(1, 9))
    new_sudoku[6][1] = str(random.randrange(1, 9))
    new_sudoku[1][8] = str(random.randrange(1, 9))
    solve(new_sudoku) 
    point = 9*9 - n
    while point != 0:
        row = random.randrange(1, 9) 
        col = random.randrange(1, 9)
        if new_sudoku[row][col] != ".":
            new_sudoku[row][col] = "."
            point -= 1
    return new_sudoku
    
    
if __name__ == '__main__':
    for fname in ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt']:
        grid = read_sudoku(fname)
        display(grid)
        solution = solve(grid)
        display(solution)